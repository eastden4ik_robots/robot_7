package ru.sviridoff.robots.robot_7.bots;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import ru.sviridoff.framework.logger.Logger;


public class Bot {

    private static String baseUrl;
    private static final Logger log = new Logger(Bot.class.getSimpleName());

    public Bot(String url) {
        baseUrl = url;
    }

    public void send(String socialMedia, String userId, String message) {
        HttpResponse<JsonNode> response = Unirest.get(baseUrl + "/" + socialMedia + "/send/{userId}/{message}")
                .routeParam("socialMedia", socialMedia)
                .routeParam("userId", userId)
                .routeParam("message", message)
                .asJson();
        log.info("Code: " + response.getStatus() + ", text: " + response.getStatusText() + ", social media: " + socialMedia);
    }

}
// baseUrl + "/{socialMedia}/notificator/send/{userId}/{message}"
package ru.sviridoff.robots.robot_7.farpost;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.sviridoff.robots.robot_7.Xpath;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Farpost_JSoup {


    public static Document logIn(String authUrl, String balanceUrl, String login, String password) throws IOException {
        Connection.Response loginForm = Jsoup.connect(authUrl)
                .method(Connection.Method.GET)
                .execute();

        Connection.Response myCabinet = Jsoup.connect(authUrl)
                .data("cookieexists", "false")
                .data("sign", login)
                .data("password", password)
                .cookies(loginForm.cookies())
                .method(Connection.Method.POST)
                .execute();
        return Jsoup.connect(balanceUrl)
                .data("cookieexists", "false")
                .cookies(myCabinet.cookies())
                .get();
    }

    public static List<String> parseCabPage(Document document) {
        List<String> list = new ArrayList<>();
        Elements elements = document.select(Xpath.currentMonth);
        for (Element element: elements.select("tr")) {
            list.add(element.text());
        }
        return list;
    }


}

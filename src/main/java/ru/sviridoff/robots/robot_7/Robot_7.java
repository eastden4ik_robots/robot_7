package ru.sviridoff.robots.robot_7;

import io.qameta.allure.model.Status;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.jsoup.nodes.Document;
import ru.sviridoff.framework.allure.AllureReporter;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Parameters;
import ru.sviridoff.robots.robot_7.bots.Bot;
import ru.sviridoff.robots.robot_7.farpost.Farpost_JSoup;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static io.qameta.allure.Allure.step;

public class Robot_7 {

    private final static Parameters params = new Parameters(System.getenv("env"));
    private final static Logger logger = new Logger(Robot_7.class.getSimpleName(), (String) params.getValue("log.file"));
    private final static Bot bot = new Bot((String) params.getValue("webhook.url"));

    private final static SimpleDateFormat sdf = new SimpleDateFormat("[dd MMMM yyyy: HH:mm:ss]");
    private final static String botName = "[Robot_7_Farpost]";


    public static void main(String[] args) {

        AllureReporter.setUp("RPA by Sviridoff. Robot 7. Проверка баланса на фарпосте.", Robot_7.class.getSimpleName());
        try {
            step("Начинаем проверку баланса на сайте https://farpost.ru");
            logger.info("Авторизуемся по URL: " + params.getValue("farpost.url.post"));
            logger.info("С логином: " + params.getValue("farpost.login"));
            logger.info("и паролем: " + params.getValue("farpost.pwd"));
            Document document = Farpost_JSoup.logIn((String) params.getValue("farpost.url.post"), (String) params.getValue("farpost.url.balance"), (String) params.getValue("farpost.login"), (String) params.getValue("farpost.pwd"));
            step("Авторизация успешна, и страница с данными получена.", Status.PASSED);
            List<String> list = Farpost_JSoup.parseCabPage(document);
            AtomicReference<String> balance = new AtomicReference<>();
            AtomicReference<String> pricePerDay = new AtomicReference<>();
            list.forEach(line -> {
                if (line.contains("Текущий баланс ") && line.split(" ").length > 2) {
                    String b = line.split(" ")[2];
                    balance.set(b);
                }
                if (line.contains("Расход по платным услугам ") && line.split(" ").length > 4) {
                    String b = line.split(" ")[4];
                    pricePerDay.set(b);
                }
            });
            list.add("Остаток дней - " + Math.round((Double.parseDouble(balance.get())/Integer.parseInt(pricePerDay.get()))));
            list.forEach(logger::success);
            step("Данные со страницы получены", () -> list.forEach(line -> step(line, Status.PASSED)));
            String message = sdf.format(new Date()) + "\n" +
                    botName + "\n" +
                    "Робот проверил баланс и остаток дней по обявлениям на сайте https://farpost.ru.\n" +
                    "[1] - " + list.get(0) + "\n" +
                    "[2] - " + list.get(1) + "\n" +
                    "[3] - " + list.get(2) + ".";
            bot.send("vk", (String) params.getValue("vk.user.id"), message);
            bot.send("tg", (String) params.getValue("tg.user.id"), message);
            step("Сообщение отправлено в телеграм", () -> {
                step(message, Status.PASSED);
            });
        } catch (Exception ex) {
            AllureReporter.processException(ex);
        } finally {
            AllureReporter.tearDown();
        }


    }

}